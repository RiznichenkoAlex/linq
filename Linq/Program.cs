﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Linq
{
    class Program
    {
        private static bool isExit = false;

        static void Main(string[] args)
        {
            Run();
        }

        public static void ShowTaskMenu()
        {
            Console.WriteLine("Task1: .NET Collections & LINQ\n\n" +
                    "1. Получить кол-во тасков у проекта конкретного пользователя (по id)\n" +
                    "2. Получить список тасков, назначенных на конкретного пользователя (по id), где name таска < 45 символов\n" +
                    "3. Получить список (id, name) из коллекции тасков, которые выполнены (finished) в текущем (2021) году для конкретного пользователя (по id)\n" +
                    "4. Получить список (id, имя команды и список пользователей) из коллекции команд, участники которых старше 10 лет, отсортированных по дате регистрации пользователя по убыванию, а также сгруппированных по командам\n" +
                    "5. Получить список пользователей по алфавиту first_name (по возрастанию) с отсортированными tasks по длине name (по убыванию)\n" +
                    "6. Получить данные о пользователе (по id) (пункт 6)\n" +
                    "7. Получить данные о проекте (пункт 7)\n" +
                    "8. Выйти\n");
            Console.Write("Введите цифру соответствующую номеру нужного пункта меню: ");
        }

        public static void ShowMainMenu()
        {
            Console.WriteLine("BSA-21\n\n" + "1. Task1\n" + "2. Выйти\n");
            Console.Write("Введите цифру соответствующую номеру нужного пункта меню: ");
        }

        public static void Run()
        {
            int inputNumber;
            bool inputCorrect;

            while (!isExit)
            {
                inputCorrect = false;
                Console.Clear();
                ShowTaskMenu();

                while (!inputCorrect)
                {
                    int inputId = 0;
                    if (int.TryParse(Console.ReadKey().KeyChar.ToString(), out inputNumber))
                    {
                        switch (inputNumber)
                        {
                            case 1:
                                Console.Clear();
                                Console.WriteLine("Введите id пользователя: ");
                                if (int.TryParse(Console.ReadLine().ToString(), out inputId) && inputId > 0)
                                {
                                    Console.Clear();
                                    //GetQualityProjectTasksByUser(inputId);
                                }
                                else
                                {
                                    InputError("Ошибка входящего параметра, попробуйте еще раз.");
                                    Console.ReadKey();
                                }
                                inputCorrect = true;
                                break;                                
                            case 2:
                                Console.Clear();
                                Console.WriteLine("Введите id пользователя: ");
                                if (int.TryParse(Console.ReadLine().ToString(), out inputId) && inputId > 0)
                                {
                                    Console.Clear();
                                    //GetTasksByUser(inputId);
                                }
                                else
                                {
                                    InputError("Ошибка входящего параметра, попробуйте еще раз.");
                                    Console.ReadKey();
                                }
                                inputCorrect = true;
                                break;
                            case 3:
                                Console.Clear();
                                Console.WriteLine("Введите id пользователя : ");
                                if (int.TryParse(Console.ReadLine().ToString(), out inputId) && inputId > 0)
                                {
                                    Console.Clear();
                                    //GetFinishedTasksByUserId(inputId);
                                }
                                else
                                {
                                    InputError("Ошибка входящего параметра, попробуйте еще раз.");
                                    Console.ReadKey();
                                }

                                inputCorrect = true;
                                break;
                            case 4:
                                Console.Clear();
                                //GetUsersListOlderTenYears();

                                inputCorrect = true;
                                break;
                            case 5:
                                Console.Clear();
                                //GetUsersListByFirstNameAsc();

                                inputCorrect = true;
                                break;
                            case 6:
                                Console.Clear();
                                Console.WriteLine("Введите id пользователя: ");
                                if (int.TryParse(Console.ReadLine().ToString(), out inputId) && inputId > 0)
                                {
                                    Console.Clear();
                                    //GetUserInfo(inputId);
                                }
                                else
                                {
                                    InputError("Ошибка входящего параметра, попробуйте еще раз.");
                                    Console.ReadKey();
                                }
                                inputCorrect = true;
                                break;
                            case 7:
                                Console.Clear();
                                //GetProjectInfo(inputId);
                                
                                inputCorrect = true;
                                break;
                            case 8:
                                Console.Write("\t\t Выход \n\n" +
                                    "Нажмите 'E' (exit) если хотите вернуться и продолжить работу или любую другую\n  клавишу для выхода : ");
                                char inputKey = Console.ReadKey().KeyChar;
                                if (!(inputKey == 'E') || !(inputKey == 'e')) isExit = true;
                                inputCorrect = true;
                                break;
                            default:
                                InputError("К сожалению было введено число, которого нет в перечне списков пунктов меню.");
                                break;
                        }
                    }
                    else
                    {
                        InputError("Был введен символ, который не является числом, попробуйте ещё раз.");
                    }
                }
            }
        }

        static void InputError(string message)
        {
            Console.Write("Ошибка ввода : {0}", message);
        }
    }
}
